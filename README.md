# WEB DEPLOY

Ce projet modèle est pour déployer son site web via le CI/CD du garage
Pour plus d'informations: [https://docs.legaragenumerique.xyz/divers/devops/cicd/](https://docs.legaragenumerique.xyz/divers/devops/cicd/)

## CONFIGURATION

- Placer son site web dans le dossier html/ :
```bash
├── Dockerfile
├── html
│   ├── content
│   │   ├── page1.html
│   │   ├── page2.html
│   │   └── page3.html
│   ├── css
│   │   └── styles.css
│   └── index.html
└── README.md
```

## PUBLICATION 

1. Créer la branche deploy :
```bash
git checkout -b deploy
```

2. Pousser la branche deploy :
```bash
git push origin -u deploy
```

3. Le site est disponible à l'adresse https://username-nom_du_projet.legaragenumerique.xyz

exemple pour l'utilisateur greg avec un projet portfolio:
> https://greg-portfolio.legaragenumerique.xyz

:warning: Ne pas modifier la Dockerfile au risque que le projet ne soit pas déployé

## MISE A JOUR

- Pour mettre à jour son projet, pousser les updates sur la branche deploy

## DEPUBLICATION

1. Basculer sur une autre branche que deploy : 
```bash
git checkout main
```

2.  Effacer la branche en local :
```bash
git branch -d deploy
```

3. Effacer la branche distante : 
```bash
git push --delete origin deploy
```

> Le site n'est plus en ligne